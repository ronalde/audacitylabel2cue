NAME			= audacitylabels2cue
VERSION			:= $(shell grep APP_VERSION= ${NAME} | awk -F\" '{print $$2}')

PREFIX			:= build/usr
DOC_DIR			:= ${PREFIX}/share/doc/${NAME}
MAN_DIR			:= ${PREFIX}/share/man/man1
BIN_DIR			:= ${PREFIX}/bin

BIN_SOURCE 		= ${NAME}
BIN_TARGET 		:= ${BIN_DIR}/${BIN_SOURCE}

README_SOURCE		= README.md
README_TARGET		:= ${DOC_DIR}/README.gz

MAN_SOURCE		= ${NAME}.1.md
MAN_TARGET		:= ${MAN_DIR}/${NAME}.1.gz

${MAN_TARGET}: ${MAN_SOURCE} ${MAN_DIR}
	pandoc $< --standalone --to man | gzip - > $@

${README_TARGET}: ${README_SOURCE} ${DOC_DIR}
	pandoc $< --standalone --to plain | gzip - > $@

${BIN_TARGET}: ${BIN_SOURCE} ${BIN_DIR}
	install --verbose --mode=0755 $< $@

${MAN_DIR} ${BIN_DIR} ${DOC_DIR}:
	mkdir -p $@

install: ${MAN_TARGET} ${CHANGELOG_TARGET} ${README_TARGET} ${BIN_TARGET}

uninstall:
	rm -rf ${PREFIX}

check:
	shellcheck ${BIN_SOURCE}
