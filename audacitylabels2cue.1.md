% AUDACITYLABELS2CUE(1) audacitylabels2cue v1.0 | Usage of "audacitylabels2cue"
% Ronald van Engelen
% April 2023

# NAME

audacitylabels2cue - convert Audacity labels file to CUE sheet 

# SYNOPSIS

**audacitylabels2cue** [**-h**, **\--help**]

**audacitylabels2cue** [**OPTARGS**] *LABELSFILE* [*AUDIOFILE*]

# DESCRIPTION

**audacitylabels2cue** is a bash (>=4) script that converts an
Audacity generated LABELSFILE to a CUESHEET, which it prints to
stdout.

# OPTIONAL ARGUMENTS

**-h**, **\--help**
:   Display usage information.

**-p** *PERFORMER*, **\--performer** *PERFORMER*
:   Uses *PERFORMER* as the corresponding CUE command before the *FILE* command; eg. 

    PERFORMER   "*PERFORMER*"\
    FILE        "*AUDIOFILE*" WAVE\
       [TRACK ...]

**-t** *TITLE*, **\--title** *TITLE*
:   Uses *TITLE* as the corresponding CUE command before the *FILE* command; eg. 

    TITLE       "*TITLE*"\
    FILE        "*AUDIOFILE*" WAVE\
       [TRACK ...]

**-l**,**--loose**
      Do not filter and/or truncate labels to make the output
      compliant to the CUESHEET standard.

**\--firsttrack** *TRACKNUMBER*
:     Integer containing the value for the number of the first
      track (eg. *2*) which defaults to 1.

**\--lasttrack** *TRACKNUMBER*
:     Integer containing the value for the number of the last
      track (eg. *2*) which defaults to 1.

**\--remarks**
:     Do add CUE *REM COMMENT* commands to indicate issues like
      violations of the CUESHEET standard.

**\--dontparsetracks**
:     Do not extract *PERFORMER* CUE commands for individual
      tracks. When ommitted, the script separates *PERFORMER* and
      *TITLE* for each line in the LABELSFILE marked with a *#*
      character, eg. *Artist Name#Track Title*.


# SEE ALSO

*README.md* **audacity (1)** **cueconvert (1)** **cueprint (1)**

The **audacitylabels2cue** source code and documentation may be downloaded
from <https://gitlab.com/ronalde/audacitylabels2cue/>.
