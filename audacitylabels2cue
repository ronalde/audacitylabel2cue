#!/usr/bin/env bash
#
## This script for linux with bash 4.x converts a labels file created
## with Audacity to a CUE sheet.
##
##  Copyright (C) 2023 Ronald van Engelen <rve+gitlab@ronalde.nl> 
##  This program is free software: you can redistribute it and/or modify
##  it under the terms of the GNU General Public License as published by
##  the Free Software Foundation, either version 3 of the License, or
##  (at your option) any later version.
##
##  This program is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##  GNU General Public License for more details.
##
##  You should have received a copy of the GNU General Public License
##  along with this program.  If not, see <http://www.gnu.org/licenses/>.
##
## Source and documentation:
## https://gitlab.com/ronalde/audacitylabel2cue
#
#
LANG=C

APP_NAME="audacitylabel2cue"
APP_VERSION="1.0"
APP_DEV_URL="https://gitlab.com/ronalde/audacitylabel2cue/"
APP_INFO_URL="${APP_DEV_URL}"

DEBUG="${DEBUG:-}"

## commandline arguments
arg_remarks=""
arg_dontparsetracks=""
arg_loose=""
arg_firsttrack=0
arg_name_first=""
arg_lasttrack=0
arg_name_last=""
declare -a meta_contents
meta_performer=""
meta_title=""
meta_file=""
meta_type="WAVE"

function print_usage() {
    err="$1"
    msg_err=""
    if [[ ${err} ]]; then
        msg_err="${c_bold}ERROR${c_std}: ${err}

"
    fi
    msg="
${msg_err}Usage:
${APP_NAME} [-p <performer>] [-t <title>] [...] LABELFILE [ AUDIOFILE ]

Generate a CUESHEET from an Audicity generated LABELFILE.

Required arguments:
  LABELFILE
      Path of the Audacity exported text file created from one or more
      'label tracks', which contains a line for each label, consisting
      of three (tab) separated values; starttime, endtime and the
      label itself.

Optional arguments:
  AUDIOFILE
      Path to the appropriate audio file. 
  -p|--performer \"PERFORMER\"
      Sets the CUE command PERFORMER in the header.
      Defaults to \`Unknown' when ommitted.
  -t|--title \"TITLE\"
      Sets the CUE command TITLE in the header.
      Defaults to \`Unknown' when ommitted.
  -l|--loose
      Do not filter and/or truncate labels to make the output
      compliant to the CUESHEET standard.
  --first TRACKNUMBER
      Integer containing the value for the number of the track (eg \`2') to start with.
      Defaults to 1 when omitted.
  --last TRACKNUMBER
      Integer containing the value for the number of the track (eg \`3') to end with.
      Defaults to 1 when omitted.
  --remarks
      Add REM COMMENT commands in the output when appropriate,
      eg. when the CUE sheet standard is violated.
  --dontparsetracks
      Do not extract PERFORMER fields for individual tracks.
      When ommitted, the script separates PERFORMER and TITLE in the
      LABELSFILE, marked with a \`#' separator mark, eg. \`Artist
      Name#Track Title'.

Version ${APP_VERSION}. For more information see:
${APP_INFO_URL}
"
    printf 1>&2 "%s\n" "${msg}"
    if [[ ${err} ]]; then
        exit 1
    else
        exit 0
    fi
}


### generic functions
function debug() {
    if [[ ${DEBUG} ]]; then
        printf 1>&2 "${c_dim}D${c_std} ${c_blue}%s${c_std}: ${c_white}%s${c_std}\n" "${FUNCNAME[1]}" "$1"
    else
        return 0
    fi
}

function set_colors() {
    ## TODO: using multiple tput calls is too expensive (0.003 per call)
    if ! tput sgr0 2>/dev/null 1>&2; then
        OPT_NOCOLOR=true
        debug "tput not supported on TERM=\`${TERM}'; OPT_NOCOLOR set"
    fi
    if [[ ${OPT_NOCOLOR} ]]; then
        c_bold="" c_dim="" c_std="" c_red="" c_green="" c_yellow="" c_blue="" c_white=""
    else        
        c_bold="$(tput bold 2>/dev/null)"
        c_dim="$(tput dim 2>/dev/null)"
        c_std="$(tput sgr0 2>/dev/null)"
        c_red="$(tput setaf 1 2>/dev/null)"
        c_green="$(tput setaf 2 2>/dev/null)"
        c_yellow="$(tput setaf 3 2>/dev/null)"
        c_blue="$(tput setaf 4 2>/dev/null)"
        c_white="$(tput setaf 7 2>/dev/null)"
    fi
    return 0
}

function die() {
    printf 1>&2 "\n${c_bold}${c_red}%s error${c_std} in ${c_bold}${c_yellow}%s${c_std}${c_dim}:${c_std}${c_white}\n%s\n\n" \
                "${APP_NAME}" \
                "${FUNCNAME[1]}" \
                "$@"
    exit 1
}


function analyze_command_line() {
    ## parse command line arguments using the `manual loop` method
    ## described in http://mywiki.wooledge.org/BashFAQ/035.
    err_int="* ${c_white}invalid argument \`${c_bold}${c_red}%s${c_std}${c_white}' \
for option ${c_bold}${c_blue}%s${c_std}; ${c_bold}should be a number${c_std}."
    while :; do
        case "${1:-}" in
            -p|--performer)
                [[ -n "${2:-}" ]] || die "Option  \`$1' needs an argument."
                meta_performer="${2}"
		shift 2
                continue
		;;
            -t|--title)
                [[ -n "${2:-}" ]] || die "Option  \`$1' needs an argument."
                meta_title="${2}"
		shift 2
                continue
		;;
            -l|--loose)
                arg_loose=true
		shift
                continue
		;;
            --*first*|--*start*)
                [[ -n "${2:-}" ]] || die "Option  \`$1' needs an argument."
                if printf -v arg_firsttrack "%d" "${2}" 2>/dev/null; then
                    debug "arg_firsttrack set to \`%d'" "${arg_firsttrack}"
                    arg_name_first="$1"
                else
                    # shellcheck disable=SC2059
                    printf -v msg_err "${err_int}" "$2" "$1"
                    die "${msg_err}"
                fi
		shift 2
                continue
		;;
            --*last*|--*end*)
                [[ -n "${2:-}" ]] || die "Option  \`$1' needs an argument."
                if printf -v arg_lasttrack "%d" "${2}" 2>/dev/null; then
                    debug "arg_lasttrack set to \`%d'" "${arg_lasttrack}"
                    arg_name_last="$1"
                else
                    # shellcheck disable=SC2059
                    printf -v msg_err "${err_int}" "$2" "$1"
                    die "${msg_err}"
                fi
		shift 2
                continue
		;;
            --dontparsetracks)
                arg_dontparsetracks=true
		shift
                continue
		;;
            --remarks)
                arg_remarks=true
		shift
                continue
		;;
	    -h|-\?|--help)
		print_usage
		;;
            --)
		shift
		break
		;;
	    -?*)
		printf "${c_bold}Notice${c_std}: unknown option \`${c_yellow}%s${c_std}' ignored.\n" "$1" 1>&2
                shift
		;;
            *)
		break
        esac
    done
    input_labelfile="$1"
    meta_file="${2:-}"
}

### label file handling

function clean_cuevalue() {
    ## TITLE and PERFORMER commands should be shorter then 81
    ## characters, and should be enclosed in double quotes if they
    ## contain spaces.
    if [[ ${arg_loose} ]]; then
        debug "arg_loose enabled; skipping"
        printf "%s" "$1"
    else
        input="${1:-${MSG_UNSPECIFIED}}"
        ## replace double quotes with single quotes
        filtered="${input//\"/\'}"
        ## limit to 80 characters including the 2 double quotes
        if (( ${#filtered} > 78 )); then
            truncated="${filtered:0:73}[...]"
        else
            truncated="${filtered}"
        fi
        printf "%s" "${truncated}"
    fi
}


function handle_filecmd() {
    if [[ "${meta_file}" ]]; then
        if [[ ! -f "${meta_file}" ]]; then
            printf '%s "%s"' "${CUE_REMCOMMENT}" \
                   "AUDIOFILE specified in value of FILE command does not exist"
        else
            lc_audiofile="${meta_file,,}"
            extension="${lc_audiofile##*.}" 
            case "${extension}" in
                wav*|flac)
                    return ;;
                aif*)
                    meta_type="AIFF" ;;
                mp3*)
                    meta_type="MP3" ;;
                *)
                    printf '%s "%s"\n%s "%s"' "${CUE_REMCOMMENT}" \
                           "${CUE_VIOLATED}: unsupported FILE extension (${extension})" \
                           "${CUE_REMCOMMENT}" "${CUE_VIOLATED}: phony TYPE" 
            esac
        fi
    else
        printf '%s "%s"' "${CUE_REMCOMMENT}" "FILE command value not specified" 
               
    fi
}


function set_metadata() {

    printf -v meta_header "generated on %(%Y-%m-%dT%H:%M)T"
    meta_header+=" using ${APP_NAME} v${APP_VERSION}." 
    [[ ${arg_remarks} ]] && meta_contents=("REM COMMENT \"${meta_header}\"")

    printf -v msg_performer "%s" "$(clean_cuevalue "${meta_performer}")"
    if (( ${#msg_performer} != ${#meta_performer} ))  && [[ ${arg_remarks} ]]; then
        print -v msg_comment '%s "%s"' "${CUE_REMCOMMENT}" "PERFORMER command value truncated"
        meta_contents+=("${msg_comment}")
    fi
    meta_contents+=("${CUE_PERFORMER} \"${msg_performer}\"")

    printf -v msg_title "%s" "$(clean_cuevalue "${meta_title}")"
    if (( ${#msg_title} != ${#meta_title} )) && [[ ${arg_remarks} ]]; then
        printf -v msg_comment '%s "%s"' "${CUE_REMCOMMENT}" "TITLE command value truncated"
        meta_contents+=("${msg_comment}")
    fi
    meta_contents+=("${CUE_TITLE} \"${msg_title}\"")
    
    rem_file="$(handle_filecmd)"
    meta_file="${meta_file:-unspecified}"
    printf -v msg_file '%s "%s" %s' "${CUE_FILE}" "${meta_file}" "${meta_type}"
    if [[ ${rem_file} ]] && [[ ${arg_remarks} ]]; then
        meta_contents+=("${rem_file}")
    fi
    meta_contents+=("${msg_file}")
    debug "$(declare -p meta_contents)"
}


function get_trackdetails() {
    ## fill TITLE field of individual track in CUE sheet.
    ## when arg_dontparsetracks is *not* set *and* the track entry in
    ## LABELFILE separates PERFORMER and TITLE with a '#' character
    ## (hardcoded below) both will be filled.
    track_label="$1"
    debug "$(declare -p arg_dontparsetracks arg_remarks)"
    if [[ ! ${arg_dontparsetracks} ]] && [[ "${track_label//#}" != "${track_label}" ]]; then
        track_performer="${track_label%%#*}"
        track_title="${track_label#*#}"
        debug "$(declare -p track_label track_title track_performer)"
        
        printf -v msg_performer "%s" "$(clean_cuevalue "${track_performer}")"
        if (( ${#msg_performer} != ${#track_performer} )) && [[ ${arg_remarks} ]]; then 
            printf '%s "%s"\n' "${CUE_REMCOMMENT}" "TRACK PERFORMER command value truncated"
        fi
        printf '%s "%s"\n' "${CUE_PERFORMER}" "${msg_performer}"

        printf -v msg_title "%s" "$(clean_cuevalue "${track_title}")"
        if (( ${#msg_title} != ${#track_title} )) && [[ ${arg_remarks} ]]; then 
                printf '%s "%s"\n' "${CUE_REMCOMMENT}" "TRACK TITLE command value truncated"
        fi
        printf '%s "%s"\n' "${CUE_TITLE}" "${msg_title}"
    else
        printf -v msg_title "%s" "$(clean_cuevalue "${track_label}")"
        printf '%s "%s"\n' "${CUE_TITLE}" "${msg_title}"
        if (( ${#msg_title} != ${#track_label} )) && [[ ${arg_remarks} ]]; then 
            printf '%s "%s"\n' "${CUE_REMCOMMENT}" "TRACK TITLE command value truncated"
        fi
    fi
}


function parse_labelfile() {
    ## iterate the LABELFILE, parsing each line as a TRACK in the
    ## CUESHEET.
    line_nr=0
    track_nr=0
    while read -r label_line; do
        ((line_nr++))
        if (( arg_lasttrack > 0 )) && (( line_nr > arg_lasttrack )); then
            debug "last track ${arg_lasttrack} reached."
            break
        fi
        if (( arg_firsttrack > 0 )) && (( line_nr < arg_firsttrack )); then
            debug "first trac ${arg_firsttrack} not reached."
            continue
        fi
        ((track_nr++))
        printf -v msg_tracknr "%02d" "${track_nr}"
        mapfile -d $'\t' -t -n 4 label_fields < <(printf "%s\t" "${label_line}")
        debug "$(declare -p track_nr msg_tracknr label_fields)"

        float="${label_fields[0]}"
        label="${label_fields[2]}"
        a_trackdetails=()
        while read -r line; do
            [[ ${line} ]] || break
            a_trackdetails+=("    ${line}")
            debug "$(declare -p a_trackdetails)"
        done< <(get_trackdetails "${label}")
        printf -v msg_trackdetails "%s\n" "${a_trackdetails[@]}"
        
        seconds="${float%%.*}"
        int_minutes=$(( seconds / 60))
        int_seconds=$(( seconds % 60))
        decimal="${float#*.}"
        ## convert 5 digit decimal (indicating microseconds/μs) from
        ## an Audacity time pinter (a float) to (correctly) rounded
        ## integer; 10#${decimal#*.} converts it to a base10 positive
        ## integer, shifting regardless of trailing zeroes.
        ## see README.md
        printf -v int_frames "%.f" "$(( ( 10**6 * 10#${decimal} ) / ( 10**6 / 75 ) ))e-6"
        if (( int_frames == 75 )); then
            ## the 75th frame indicates the next second
            int_frames="0"
            ((int_seconds++))
            if (( int_seconds > 59 )); then
                int_seconds=0
                ((int_minutes++))
            fi
        fi
        printf -v msg_start "%02d:%02d:%02d" "${int_minutes}" "${int_seconds}" "${int_frames}"
        debug "$(declare -p msg_start)"

        msg_warning=""
        if [[ ${arg_remarks} ]]; then 
            if (( int_minutes > 99 )); then
                ## shellcheck disable=SC2059
                printf -v msg_warning '    %s "%s"\n' "${CUE_REMCOMMENT}" "${CUE_VIOLATED}: INDEX minute count (${int_minutes}) exceeds maximum (99)"
            fi
        fi
        msg_track="\
  TRACK ${msg_tracknr} AUDIO
${msg_trackdetails}\
${msg_warning}\
    ${CUE_INDEX} ${msg_start}"
        
        printf "%s\n" "${msg_track}" 
    done < "${input_labelfile}"
    if (( track_nr == 0 )); then
        #  in \`${input_labelfile}'
        msg="* counted ${c_bold}${line_nr} tracks${c_std}, \
but argument (\`${c_bold}${c_red}${arg_firsttrack}${c_std}') \
for option ${c_bold}${c_blue}${arg_name_first}${c_std}${c_bold} \
set to a greater value.${c_std}"
        die "${msg}"
    fi
}


function print_tracks() {
    parse_labelfile || die "could not parse label file"
}

function print_meta() {
    set_metadata
    printf "%s\n" "${meta_contents[@]}"
}

## output formatting
# shellcheck disable=SC2034
c_bold="" c_dim="" c_std="" c_red="" c_yellow="" c_blue="" c_green="" c_white=""
set_colors

## fixed strings for CUE commands
printf -v CUE_PERFORMER "%-11s" "PERFORMER"
printf -v CUE_TITLE "%-11s" "TITLE"
printf -v CUE_FILE "%-11s" "FILE"
printf -v CUE_INDEX "%-11s" "INDEX 01"
printf -v CUE_REMCOMMENT "%-11s" "REM COMMENT"
CUE_VIOLATED="CUESHEET VIOLATION"
MSG_UNSPECIFIED="(unspecified)"

## start
analyze_command_line "$@"

[[ "${input_labelfile}" ]] || print_usage "no input LABELFILE specified."
[[ -f "${input_labelfile}" ]] || print_usage "specified input LABELFILE \`$1' is not found."

print_meta || die "could not generate meta fields"
print_tracks
