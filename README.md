# README

**audacitylabels2cue** is a bash (>=4) script that converts a
**LABELSFILE** created with Audacity[^audacity] to a **CUESHEET**
format[^cuesheet] which it will print to stdout.

## Installation and usage

Apart from bash the script has no dependencies so it can be started
directly after downloading it to the current working directory:

```bash
wget "https://gitlab.com/ronalde/audacitylabel2cue/-/raw/main/audacitylabels2cue"
bash audacitylabels2cue
```

Or, make it executable:

```bash
chmod +x audacitylabels2cue
./audacitylabels2cue
```

Or by temporary downloading it, eg. run it straight from the web:

```bash
bash <(wget -q -O - "https://gitlab.com/ronalde/audacitylabel2cue/-/raw/main/audacitylabels2cue") --help
# or
bash <(curl -s "https://gitlab.com/ronalde/audacitylabel2cue/-/raw/main/audacitylabels2cue") -h
```

Each of the invocations above (so without argument, or with the
**--help** or **-h** argument) displays the usage info:

```
Usage:
audacitylabel2cue [-p <performer>] [-t <title>] [...] LABELFILE [ AUDIOFILE ]
```

## Let's Make Bash Float!

Audacity uses a precision of a microsecond (μs), or a million portions
of a second (eg. 1/1000000s == 0.0000001s == 0.0001ms == 1μs) to
indicate a point in time, which is of course rather course considering
time and sound are per (analogue) definition continuous.

A CUESHEET **INDEX** command uses a `mm:ss:ff` time indicator, where
the `ff` portion indicates a *timecode frame*, which consists of
1/75th of a second (=~ 0.0133333s =~ 13ms =~ 13000μs). This even
courser division of time originates from the *MSF addressing scheme*
from the**REDBOOK** standard , eg. the original physical ("1x") read
speed of CD Audio discs at 75 sectors per second.

   **NOTE**: The precision of time/frame indicators has no relation
             with the precision of the sampling of the coded
             audio. For instance; the REDBOOK encoding rate equals
             44.100Hz.  So one sample is taken each 1/44000th second
             (=~ 0.0000226s =~ .0226 ms =~ 22.6μs).  When the CD
             player reads one sector however, which it does every 1/75
             second (=~ 0.0133333s =~ 13 ms =~ 13000μs), it stores (
             (1/75) / (1/44100) = ) 588 samples in memory. The courser
             the frame count per time unit and the higher the sampling
             rate for that unit, the more samples should be read in
             per frame, and in that time unit.\
             Before the next read cycle, it's up to the accuracy of
             the electronical induced resonance of the audio player's
             builtin crystal (and therefore the quality and stability
             of it's power supplies and things like noise and EMI etc)
             to evenly spread the time between offering each of those
             consecutive samples to it's (builtin) AD converter. Which
             for REDBOOK should be as close to 22.5792mHz (22.5792 *
             1000000 / 512 = 44100Hz) as possible. See [How an Atomic
             Clock Really Works: Inside the HP 5061A Cesium
             Clock](https://www.youtube.com/watch?v=eOti3kKWX-c) for
             more information on this subject, which is (also)
             essential for (understanding) digital music chains.


The script uses the well known capabilities of bash regarding string
manipulation, and its rather obscure, but surprisingly apt internal
arithmatic to translate the decimal portion (eg. `[0.]079300`,
indicating microseconds/μs) from an Audacity time pointer
(eg. `61.079300`) to an integer representing the timecode frame
(eg. `[0.]079300 / 75`) using this one liner:

```bash
printf -v int_frames "%.f" "$(( ( 10**6 * 10#${decimal} ) / ( 10**6 / 75 ) ))e-6"
```

1. before this, the integer part of the float and its separator
   (eg. `[61][.]079300`, which integer (`61`) is used elsewhere to get
   the prelimenary minute (`mm`) and second (`ss`) count) are filtered
   out using string manipulation (`decimal="${start#*.}"`), to keep
   only the portion behind the decimal separator containing the number
   of microseconds in a 6 digit string (`079300`)
2. next, it converts/forces (`10#${decimal}`) that string to a base10
   integer (`79300`)
3. than it multiplies that integer by a million (`10**6 *
   10#${decimal}`, the `x**y` indicates the power of, ie `x^y`) to
   make sure any subsequent truncating due to bash' inability to
   handle decimals which will happen in the division ahead, will take
   place outside of our microsecond range (ie. in the nanosecond
   range)
4. now the real division takes place, using the same supersizing method for the divider (`10**6 / 75 `)
5. the truncated result of that division will be fed back to `printf`
   as a fraction of our power (`e-6`), to make sure it always formats
   the result as a two digit (`ff`) notation, padding it with a zero
   when needed (<10, so `1` will become `01`).

Finally, the function returns the correctly rounded framenumber as an
integer to the calling function, which uses that to determine if the
seconds should be raised, when the framenumber equals `75`, which in 
turn can cause the need to also raise the minutes, when the
secondnumber equals `60`.


### Examples

Given the following **LABELSFILE** which is [generated using
Audacity](#generating-a-labelsfile-in-audacity):

```
## duke-labels.txt (numbers are phony)
0.000000	61.793016	Niven#Introduces the live performance at the Newport Festival (july 4th, 1959)
61.079300	691.390021	01. Skin Deep
691.390021	1150.864559	02. Jones
1150.864559	1589.998010	Niven#Introduces the live performances of The Duke in Blue Note Club sessions (august 1959)
1499.988888	1786.234785	01. Take the "A" train (JUST ONE FRAME BEFORE 25:00:00)
5999.998010	2631.553741	02. Flirtibird         (THIS ONE JUST MADE THE FLIP)
```

The script will translate that to the following **CUESHEET**:

```
#./audacitylabels2cue duke-labels.txt

PERFORMER   "unspecified"
TITLE       "unspecified"
FILE        "unspecified" WAVE
  TRACK 01 AUDIO
    PERFORMER   "Niven"
    TITLE       "Introduces the live performance at the Newport Festival (july 4th, 1959)"
    INDEX 01    00:00:00
  TRACK 02 AUDIO
    TITLE       "01. Skin Deep"
    INDEX 01    01:01:06
  TRACK 03 AUDIO
    TITLE       "02. Jones"
    INDEX 01    11:31:29
  TRACK 04 AUDIO
    PERFORMER   "Niven"
    TITLE       "Introduces the live performances of The Duke in Blue Note Club sessions ([...]"
    INDEX 01    19:10:65
  TRACK 05 AUDIO
    TITLE       "01. Take the 'A' train (JUST ONE FRAME BEFORE 25:00:00)"
    INDEX 01    24:59:74
  TRACK 06 AUDIO
    TITLE       "02. Flirtibird         (THIS ONE JUST MADE THE FLIP)"
    INDEX 01    100:00:00
```

Adding command line arguments will produce the following:

```
#./audacitylabels2cue --remarks -p "The Duke" -t "The David W. Niven Collection of Early Jazz Legends, 1921-1991 - Tape #75: Duke Ellington in Summer 1959" duke-labels.txt Duke\ Ellington\ And\ His\ Orchestra\ -\ Side\ A.flac

REM COMMENT "generated on 2023-04-13T22:08 using audacitylabel2cue v0.2."
PERFORMER   "The Duke"
REM COMMENT "TITLE command value truncated"
TITLE       "The David W. Niven Collection of Early Jazz Legends, 1921-1991 - Tape #75[...]"
FILE        "Duke Ellington And His Orchestra - Side A.flac" WAVE
  TRACK 01 AUDIO
    PERFORMER   "Niven"
    TITLE       "Introduces the live performance at the Newport Festival (july 4th, 1959)"
    INDEX 01    00:00:00
  TRACK 02 AUDIO
    TITLE       "01. Skin Deep"
    INDEX 01    01:01:06
  TRACK 03 AUDIO
    TITLE       "02. Jones"
    INDEX 01    11:31:29
  TRACK 04 AUDIO
    PERFORMER   "Niven"
    REM COMMENT "TRACK TITLE command value truncated"
    TITLE       "Introduces the live performances of The Duke in Blue Note Club sessions ([...]"
    INDEX 01    19:10:65
  TRACK 05 AUDIO
    TITLE       "01. Take the 'A' train (JUST ONE FRAME BEFORE 25:00:00)"
    INDEX 01    24:59:74
  TRACK 06 AUDIO
    TITLE       "02. Flirtibird         (THIS ONE JUST MADE THE FLIP)"
    REM COMMENT "CUESHEET VIOLATION: INDEX minute count (100) exceeds maximum (99)"
    INDEX 01    100:00:00
```

## Usage examples / Lab

### Preparation

1. Set your preferences
```bash
project="duke1959"
performer="Duke Ellington & His Orchestra"
title="Live in Summer 1959 (Tape #75 by David W. Niven)"
albumname="${performer} - ${title}"
```
2. Prepare your (sticky) working directory and files
```bash
## make these stick
workdir="${HOME}/${project}"
mkdir -p "${workdir}"
audio_name="${project}-audio.flac"
audio_path="${workdir}/${audiofile_name}"
baseurl="https://archive.org/download"
srcdir="Duke_Ellington_Tape_76_1959_Newport_cont_Blue_Note_Live_1959"
srcfile="Duke_Ellington_Tape_76_1959_Newport_cont_Blue_Note_Live_1959_Side_A.flac"
## download the big file (~280MB) one time
[[ -f "${audio_path}" ]] || wget -O "${audio_path}" "${baseurl}/${srcdir}/${srcfile}"
## make a symbolic link (if not already done) to indicate it's source
[[ -L "${workdir}/${srcfile}" ]] || ( cd "${workdir}" && ln -s "${audio_name}" "${srcfile}" )
```

3. Make a temporary directory which you can throw away after each experiment
```bash
tempdir=$(mktemp -d "/tmp/${project}.XXXXX")
cuesheetname="${project}-cuesheet.cue"
cuesheetfile="${tempdir}/${project}-cuesheet.cue"
labelsfile="${tempdir}/${project}-label.cue"
## generate the labelsfile for the first three tracks:
labels="0.000000	61.793016	Niven#Introduction Newport Festival (july 4th, 1959)
61.079300	691.390021	01. Skin Deep
691.390021	1150.864559	02. Jones"
printf "%s\n" "${labels}" > "${labelsfile}"
## generate the CUESHEET using this script
scripturl="https://gitlab.com/ronalde/audacitylabel2cue/-/raw/main/audacitylabels2cue"
bash <(wget -q -O - "${scripturl}") -p "${performer}" -t "${title}" "${labelsfile}" "${audio_name}" > "${cuesheetfile}"
```

Check the results so far:

```bash
declare -p workdir
ls -la "${workdir}"
```

Should show something like:

```
# total 273248
# drwxr-xr-x 2 username username      4096 Apr 14 09:43 .
# drwxr-xr-x 3 username username      4096 Apr 14 09:16 ..
# lrwxrwxrwx 1 username username        19 Apr 14 09:34 Duke_Ellington_Tape_76_1959_Newport_cont_Blue_Note_Live_1959_Side_A.flac -> duke1959-audio.flac
# -rw-r--r-- 1 username username 279791969 May 29  2013 duke1959-audio.flac
```

```bash
declare -p tempdir
ls -la "${tempdir}"
## should show something like:
# total 8
# drwx------  2 username username  80 Apr 14 09:36 .
# drwxrwxrwt 23 root     root     680 Apr 14 09:35 ..
# -rw-r--r--  1 username username 422 Apr 14 09:36 duke1959-cuesheet.cue
# -rw-r--r--  1 username username 140 Apr 14 09:35 duke1959-label.cue

declare -p cuesheetfile
cat  "${cuesheetfile}"
## should show something like:
# PERFORMER   "Duke Ellington & His Orchestra"
# TITLE       "Live in Summer 1959 (Tape #75 by David W. Niven)"
# FILE        "duke1959-audio.flac" WAVE
#  TRACK 01 AUDIO
#  [continued]

declare -p workdir
```

### Playing individual 'tracks' from a CUESHEET using mpd

To use these files in mpd (assuming it's running on the local pc, and it uses 
`/etc/mpd.conf` as its configuration file):

```bash
## get the music_directory
mpdconf="/etc/mpd.conf"
[[ -f "${mpdconf}" ]] || echo "STOP: mpdconf \`${mpd_conf}' not found."

re="music_directory[[:space:]]+['\"]+([^['\"]+)['\"]+$"
while read -r line; do if [[ "${line}" =~ ${re} ]]; then mpd_musicdir="${BASH_REMATCH[1]}"; break; fi; done < "${mpdconf}"
[[ -d "${mpd_musicdir}" ]] || echo "STOP: mpd_musicdir "${mpd_musicdir}" does not exist." && echo "using ${mpd_musicdir}"

## copy the files to it (if its found)
sudo install -m 755 -d "${mpd_musicdir}/${project}"
sudo install -m 644 "${cuesheetfile}" "${audiofile}" "${mpd_musicdir}/${project}/"
## make sure they're accessible to mpd
sudo chmod 644 "${mpd_musicdir}/${cuesheetname}" "${mpd_musicdir}/${audiofile}"
## update mpd's database
mpc update
## load the cuesheet in the playlist 
mpc load ${cuesheetname}
## play track 2 ("01. Skin Deep"); 
mpc play 02
```

### Using shnsplit to split tracks according to the CUESHEET 

Together with `shnsplit`[^shnsplit] the script can be used to split
**AUDIOFILE** in individual track files[^zerocrossings]:

```bash
bash <(wget -q -O - "${scripturl}") -p "${performer}" -t "${title}" "${labelsfile}" "${audiofile}" | \
   shnsplit -q -- "${audiofile}"
```



### Using cuetag to metatag tracks splitted by shnsplit

For a complete workflow with (meta)tagging using `cuetag.sh`[^cuetools]
of the `shnsplit` generated (single track) files, one must first save
the output of the script to a file, and make `shntools` output `flac`
files because cuetools doesn't (yet?) support `aiff`:

```bash
bash <(wget -q -O - "${scripturl}") \
   -p "${performer}" -t "${title}" "${labelsfile}" "${audiofile}" > "${cuesheetfile}"
shnsplit -d "${tempdir}" -t "%n. %t" -o flac -f "${cuesheetfile}" -- "${audiofile}"
cuetag.sh "${cuesheetfile}" "${tempdir}"/*.flac
```

## Background & rationale

### Why use (old fashioned) CUESHEET?

While listening through `mpd`[^mpd] to some downloaded CD-length recordings from
the *Internet Archive*[^niven] it hindered me that I couldn't quickly
skip between tracks.

By creating a **CUESHEET** file for that **AUDIOFILE** [`mpd` and
(some of) its clients allow you to treat those (virtual) tracks as
though they were real individual
audiofiles](#playing-individual-tracks-from-a-cuesheet-using-mpd),
while the **CUESHEET** file itself is handled like a normal directory.
One of those clients is
[`ncmpcpp`](https://github.com/ncmpcpp/ncmpcpp).

And such a **CUESHEET** can be handy for [other
purposes](#splitting-tracks-while-metatagging-them-with-cuetag)
as well ...

### Why not use one of the (many) existing tools/scripts?

Because the scripts have dependencies (like perl, python or ruby),
while the programs require compiling.

And not all seem to be that great. For instance the 2011 ruby script
[`label2cue`](https://github.com/hornc/Label-to-Cue/blob/master/label2cue.rb)
doesn't adhere to the CUESHEET standard and doesnt't properly round
frames, and omits consecutive rounding of the seconds and thus
minutes altogether.

```ruby
secs = 5999.998010       ## (input from labels file)
s = secs.to_i            ## == 5999
#f = secs.to_f           ## (nonsense; secs is float)
#decimal = secs.to_f - s ## (nonsense; why init f, and secs was already float)
decimal = secs - s       ## =~    0.99801 (actually:  0.998010000000022????)
frames = decimal * 75    ## =~   74.85075 (actually: 74.850750000018????)
"%02d:%02d:%02d" % [s / 60, s % 60, 75 * (f - s)] 
## == "99:59:74"
##     ^^ ^^ ^^ <------- all three values wrong
```

In reality, this leads to an INDEX time pointer which is off a maximum
of ('only') 13ms.


### Generating a LABELSFILE in Audacity

1. Open an **AUDIOFILE** in Audacity
2. Choose *Label Track* from *Add* in the *Tracks* menu

Then, for each individual (future/wanted) CUE TRACK:

1. Place the cursor roughly on the wanted (start-)position in the
   Wave-display of the audio track
2. Press <kbd>Z</kbd> (or choose *At Zero Crossings*[^zerocrossings] in the *Select* menu)
   to make Audicity point the cursor to a nearby sample where both the
   Left and Right channels have zero amplitude, and to make it zoom in
   on a very small range of single samples
3. Press <kbd>Ctrl+b</kbd> (or select *Add Label at Selection* in the *Labels*
   submenu of the *Edit* menu) to add a label on that exact position
4. Type a label (like `01. Skin Deep` in the example above)
5. Repeat for each label.

   **NOTE**: as CUE files only use start frames, the ending position
             (and range length) are ignored by the script. See the
             [Label
             Tracks](https://manual.audacityteam.org/man/label_tracks.html)
             page on the Audacity wiki for more information.

To create **LABELSFILE**, choose *Export Labels* from the *Export* item
in the *File* menu.



### Notes

[^cuesheet]: A *cuesheet* is a metadata file which describes how the
             tracks of a digital album or file are laid out.
             See: https://en.wikipedia.org/wiki/Cue_sheet_(computing)

[^audacity]: Audicity is free and open source, cross-platform audio software. 
             See https://www.audacityteam.org/

[^mpd]:      [`mpd`](https://www.musicpd.org/), the free and open source 
             *music player daemon*, supports `CUESHEETs` since version 1.7. 
             Search for `cue` in the projects' 
             [NEWS](https://github.com/MusicPlayerDaemon/MPD/blob/master/NEWS) 
             file to get an overview of the development of that feature.
             Also see: [Supporting cue files in mpd: syntactic and semantic consistency with standard formats for audio metadata](https://bitfreedom.noblogs.org/files/2019/09/mpdcuetags-v2.0.pdf)

[^shnsplit]: `shnsplit` is a command from the `shntools` packages. 
             See: http://shnutils.freeshell.org/shntool/

[^cuetools]: `cuetag.sh` is part of the `cuetools` package.
             See: https://github.com/svend/cuetools/blob/master/src/tools/cuetag.sh 

[^zerocrossings]: Although popular believe dictates that
                  **non-zero-crossings** in audio cutting are the
                  (only or main) reason for audible clicks and pops,
                  they're not. To prevent those unwanted artifacts use
                  short steep fade-ins and outs at the beginning/end
                  of each track. Serious audio editors let
                  you do that automatically. **Ardour** for example,
                  which also can generate CUE sheets in the same go,
                  see [Export Format Profiles
                  ](https://manual.ardour.org/exporting/edit-export-format-profile/).

[^niven]: The Duke part from the
    [The David W. Niven Collection of Early Jazz Legends, 1921-1991](https://archive.org/details/davidwnivenjazz).

<!--
          >>>
          **NOTE ON NIVEN COLLECTION**: Said[^niven] recordings of
          live performances by the Duke in the summer of 1959, in july on the
          Newport Jazz Festival and in august in the Blue Note Club on [Tape
          #75](https://archive.org/details/Duke_Ellington_Tape_76_1959_Newport_cont_Blue_Note_Live_1959)
          (wrongfully labeled as "Tape 76") turned out to be quite
          disappointing. Not only are Niven's 'original' recordings simple
          personal rips of individual tracks of his own (**AAD**) CD's, like
          [Duke Ellington Live! At The Newport Jazz Festival '59 (EmArcy – 842
          071-2)](https://www.discogs.com/release/9602970-Duke-Ellington-Duke-Ellington-Live-At-The-Newport-Jazz-Festival-59)
          to cassette tape, cancelling the [apparent quality of the second
          `A`](https://www.highfidelity.pl/!ev/artykuly/17_06_2008/nagra.html),
          a high grade 1997 [Nagra
          PL-P](https://www.nagraaudio.com/product/pl-p/).
     
          Furthermore, the archiver played back the Niven-produced
          (**AADA**) cassette tapes in an el cheapo TEAC casette player, to
          digitize it (once more) with a noisy and jittery Soundblaster card in
          his noisy and jittery PC. The resulting (**AADAAD**) files are simply
          horrible, containing loud noise, hum and distortions which are all
          emphasized by all kinds of digital artificats due to the final
          'mastering'.

          On the other hand the effort Niven and the archiver put in this is astonishing.
          And it's of course great that one can get to know what's
          one those early recording for free, before one decides to look for
          properly remastered albums, or takes the time to manually fix the most
          obvious showstoppers, which is rather easy using Audacity's builtin
          noise cancellation, high pass (>40Hz) and normalization filters.
          >>>
-->
